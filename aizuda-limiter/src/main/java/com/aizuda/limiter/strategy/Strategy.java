/*
 * 爱组搭 http://aizuda.com 低代码组件化开发平台
 * ------------------------------------------
 * 受知识产权保护，请勿删除版权申明
 */
package com.aizuda.limiter.strategy;

/**
 * 策略枚举
 * <p>
 * 尊重知识产权，CV 请保留版权，爱组搭 http://aizuda.com 出品
 *
 * @author imantou
 * @since 2022-6-13
 */
public enum Strategy {
    /**
     * 默认策略
     * <p>
     * 默认执行的key策略，获取方法全限定类名
     */
    DEFAULT,
    /**
     * ip策略
     * <p>
     * 通过ip进行限流
     */
    IP,
    /**
     * 幂等性策略
     * <p>
     * 重排序参数使用md5加密参数对比的方式实现幂等性校验
     */
    IDEMPOTENCY
}